module.exports = {
    notFoundMiddleware: require('./not-found.middleware'),
    errorMiddleware: require('./error.middleware')
}