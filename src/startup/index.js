const express = require('express');

let _express = null;
let _config = null;

// Clase de inicio de la aplicación
class Server {
    constructor({ config, router }){
        _config = config;
        _express = express().use(router);
        // _express = express().use(router);
    }

    start() {
        return new Promise(resolve => {
            debugger
            _express.listen(_config.PORT, () => {
                console.log(_config.APPLICATION_NAME + ' API rounning on port: ' + _config.PORT);
                
                resolve();
            });
        });
    }
}

module.exports = Server;