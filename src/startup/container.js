// container para configurar contenedor de inyección de dependencias - awilix
const { createContainer, asClass, asValue, asFunction } = require('awilix');

// config - para inyecctar variables de entorno
const config = require('../config');
const app = require('.');

// services
const { HomeService, CommentService, IdeaService, UserService } = require('../services');

// controllers
const { HomeController } = require('../controllers')

// routes
const { HomeRoutes } = require('../routes/index.routes');
const Routes = require('../routes');

// models
const { User, Comment, Idea } = require('../models');

// repositories
const { UserRepository, IdeaRepository, CommentRepository } = require('../repositories');

// Helpers
const { ErrorHelper } = require('../helpers');

const container = createContainer();

container
    .register({
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
    })
    .register({
        HomeService: asClass(HomeService).singleton(),
        UserService: asClass(UserService).singleton(),
        IdeaService: asClass(IdeaService).singleton(),
        CommentService: asClass(CommentService).singleton()
    })
    .register({
        HomeController: asClass(HomeController.bind(HomeController)).singleton() // se usa bind porque express cambia el scope cuando llama un controlador
    })
    .register({
        HomeRoutes: asFunction(HomeRoutes).singleton()
    })
    .register({
        User: asValue(User),
        Idea: asValue(Idea),
        Comment: asValue(Comment)
    })
    .register({
        UserRepository: asClass(UserRepository).singleton(),
        IdeaRepository: asClass(IdeaRepository).singleton(),
        CommentRepository: asClass(CommentRepository).singleton()
    })
    .register({
        ErrorHelper: asClass(ErrorHelper)
    });

module.exports = container;