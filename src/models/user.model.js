const mongoose = require('mongoose');
const { Schema } = mongoose;
// compareSync: compara contraseñas encriptadas.
// hashSync: genera hash para contraseña. genSaltSync: genera salt para la contraseña.
const { compareSync, hashSync, genSaltSync } = require('bcryptjs');

const UserSchema = new Schema({
    name: {type: String, required: true},
    username: {type: String, required: true},
    password: {type: String, required: true},
});

UserSchema.methods.toJson = function() {
    let user = this.toObject();
    delete user.password;
    return user;
};

UserSchema.methods.comparePasswords = function(password) {
    return compareSync(password, this.password);
};

// Se aplica esta lógica cada vez que se salve un doc de mongoose.
UserSchema.pre('save', async function(next){
    const user = this;

    if (user.isModified('password')) {
        return next();
    }

    const salt = genSaltSync(10);
    const hashedPassword = hashSync(user.password, salt);
    user.password = hashedPassword;
    next();
});

module.exports = mongoose.model('user', UserSchema)