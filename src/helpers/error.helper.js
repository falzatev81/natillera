class ErrorHelper {

    badRequest() {
        const error = new Error();
        error.status = 400;
        error.message = 'resource must sent';
        throw error;
    }

    notFound() {
        const error = new Error();
        error.status = 404;
        error.message = 'resource does not found';
        throw error;
    }
}

module.exports = ErrorHelper;  