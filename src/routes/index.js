const express = require('express');
const cors = require('cors');
// Helmet es un middleware que cierras algunas brechas de seguridad.
const helmet = require('helmet');
const compression = require('compression');
// ayuda a capturar exepciones asincronas
require('express-async-errors');
const { notFoundMiddleware, errorMiddleware } = require('../middlewares');

module.exports = function({ HomeRoutes }) {
    const router = express.Router();
    const apiRoutes = express.Router();

    apiRoutes
        .use(express.json())
        .use(cors())
        .use(helmet())
        .use(compression());

    apiRoutes.use('/home', HomeRoutes);

    router.use('/v1/api', apiRoutes);

    router.use(notFoundMiddleware);
    router.use(errorMiddleware);

    return router;
}