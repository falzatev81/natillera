let _homeService = null;

class HomeController {
    // Inyección de dependencias hecha por awilix - El nombre en el construcctor debe coincidir con el nombre en el
    // register del startup/container
    constructor({ HomeService }) {
        // no se usa this._homeService para que propiedad sea de tipo privado y que solo se acceda desde la clase.
        _homeService = HomeService;
    }

    index(req, res) {
        return res.send(_homeService.index());
    }
}

module.exports = HomeController;