const BaseService = require('./base.service');
// const ErrorHelper = require('../helpers/error.helper')

let _ideaRepository = null;
let _errorHelper = null;

class IdeaService extends BaseService {
    constructor({ IdeaRepository, ErrorHelper }){
        super(IdeaRepository);
        _ideaRepository = IdeaRepository;
        _errorHelper = ErrorHelper;
    }

    async getUserIdeas(author) {
        if (!author) {
            _errorHelper.badRequest();
        }
        return await _ideaRepository.getUserIdeas(author);
    }

    async upvoteIdea(ideaId) {
        if (!ideaId) {
            _errorHelper.badRequest();
        }

        const idea = await _ideaRepository.get(ideaId);

        if (!idea) {
            _errorHelper.notFound();
        }

        idea.upvotes,push(true);

        return await _ideaRepository.update(ideaId, { upvotes: idea.upvotes });
    }

    async downvoteIdea(ideaId) {
        if (!ideaId) {
            _errorHelper.badRequest();
        }

        const idea = await _ideaRepository.get(ideaId);

        if (!idea) {
            _errorHelper.notFound();
        }

        idea.downvotes,push(true);

        return await _ideaRepository.update(ideaId, { downvotes: idea.downvotes });
    }
}

module.exports = IdeaService;