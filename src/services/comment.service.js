const BaseService = require('./base.service');

let _commentRepository = null;
let _ideaRepository = null;
let _errorHelper = null;

class CommentService extends BaseService {
    constructor({ CommentRepository, ErrorHelper, IdeaRepository }){
        super(CommentRepository);
        _commentRepository = CommentRepository;
        _ideaRepository = IdeaRepository;
        _errorHelper = ErrorHelper;
    }

    async getIdeaComments(ideaId) {
        if (!ideaId) {
            _errorHelper.badRequest();
        }

        const idea = await _ideaRepository.get(ideaId);

        if (!idea) {
            _errorHelper.notFound();
        }

        const { comments } = idea;
        return comments;
    }

    async createComment(comment, ideaId) {
        if (!ideaId) {
            _errorHelper.badRequest();
        }

        const idea = await _ideaRepository.get(ideaId);

        if (!idea) {
            _errorHelper.notFound();
        }

        const createdComment = await _commentRepository.create(comment);
        idea.comments.push(createdComment);

        return await _ideaRepository.update(ideaId, { comments: idea.comments });
    }
}

module.exports = CommentService;