let _errorHelper = null;

class BaseService {
    constructor(repository, ErrorHelper){
        this.repository = repository;
        _errorHelper = ErrorHelper;
    }

    async get(id){
        if (!id) {
            _errorHelper.badRequest();
        }

        const currentEntity = await this.repository.get(id);

        if (!currentEntity) {
            _errorHelper.notFound();
        }

        return currentEntity;
    }

    async getAll() {
        return await this.repository.getAll();
    }

    async create(Entity) {
        return await this.repository.create(Entity);
    }

    async uptdate(id, entity) {
        if (!id) {
            _errorHelper.badRequest();
        }

        return await this.repository.uptdate(id, entity);
    }

    async delete(id) {
        if (!id) {
            _errorHelper.badRequest();
        }

        return await this.repository.delete(id);
    }
}

module.exports = BaseService;